# Teste para Java Developer

[![Lyncas Logo](https://img-dev.feedback.house/TCo5z9DrSyX0EQoakV8sJkx1mSg=/fit-in/300x300/smart/https://s3.amazonaws.com/feedbackhouse-media-development/modules%2Fcore%2Fcompany%2F5c9e1b01c5f3d0003c5fa53b%2Flogo%2F5c9ec4f869d1cb003cb7996d)](https://www.lyncas.net)
### Requisitos

- Spring boot (https://spring.io/projects/spring-boot)
- Google books (https://developers.google.com/books/) não utilizar lib

### Diferencial

- Testes unitários
- Arquitetura do projeto
- Backend e frontend em microserviços distintos

## Como participar?

1. Faça um clone deste repositório.
2. Quando estiver finalizado a prova, compartilhe seu código no seu Git de preferência e nos envie para o e-mail que consta ao final desse documento.
3. Faremos nossa análise e te daremos um retorno.

## Detalhes da prova

### Critérios analisados

- Arquitetura do projeto (camadas)
- Aplicação de orientação a objeto
- Funcionalidades e funcionamento

### O que você deve desenvolver

- A prova consiste em criar uma interface Web para interação com o Google books.
- Seu projeto deve também conter um arquivo README com a explicação das tecnologias utilizadas e as instruções para rodar.
- Descrever suas dificuldades e facilidades, bem como o número de horas de desenvolvimento.

### Funcionalidades

A App deve conter as seguintes funcionalidades:

1. Menus - Pesquisar / Favoritos (default)
2. Pesquisar livros no Google books e exibir em tela (com a foto, descrição e título do livro)
3. Salvar livros em uma lista de "meus favoritos" (localmente na base escolhida)
4. Permitir excluir livros favoritos
5. Exibir alerta de confirmaçao de exclusão

### Especificações técnicas

* O App deve se comunicar com o Google Books API pelo backend Java, utilizando HttpClient sem o uso das libs do GoogleBooks.

## Dúvidas? Envio da prova?
`testes@lyncas.net`

### Desde já obrigado pelo seu interesse e boa prova!